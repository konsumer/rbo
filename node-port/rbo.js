// shitty python built-ins, purpose-built for just score's usage
const range = (size = 0) => (new Array(size)).fill().map((i, v) => v)
const sorted = (list) => list.sort((a, b) => a[0] - b[0])

// JS port of https://github.com/ragrawal/measures/blob/master/measures/rankedlist/RBO.py
const score = (l1 = [], l2 = [], p = 0.98) => {
  const [sl, ll] = sorted([[l1.length, l1], [l2.length, l2]])
  const [s, S] = sl
  const [l, L] = ll
  if (s === 0) return 0

  // Calculate the overlaps at ranks 1 through l
  // (the longer of the two lists)
  const ss = new Set([]) // contains elements from the smaller list till depth i
  const ls = new Set([]) // contains elements from the longer list till depth i
  const x_d = {0: 0}
  let sum1 = 0.0
  range(l).forEach(i => {
    const x = L[i]
    const y = (i < s) ? S[i] : undefined
    const d = 1 + i

    // if two elements are same then
    // we don't need to add to either of the set
    if (x === y) {
      x_d[d] = x_d[i] + 1.0
    // else add items to respective list
    // and calculate overlap
    } else {
      ls.add(x)
      if (y !== undefined) ss.add(y)
      x_d[d] = x_d[i] + (ss.has(x) ? 1.0 : 0.0) + (ls.has(y) ? 1.0 : 0.0)
    }
    // calculate average overlap
    sum1 += x_d[d] / d * Math.pow(p, d)
  })

  let sum2 = 0.0
  range(l - s).forEach(i => {
    const d = s + i + 1
    sum2 += x_d[d] * (d - s) / (d * s) * Math.pow(p, d)
  })
  const sum3 = ((x_d[l] - x_d[s]) / l + x_d[s] / s) * Math.pow(p, l)

  // Equation 32
  const rbo_ext = (1 - p) / p * (sum1 + sum2) + sum3
  return rbo_ext
}

module.exports = score
