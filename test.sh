#!/usr/bin/env bash

DIRECTORY=$(cd `dirname $0` && pwd)

echo Python
python "${DIRECTORY}/python/RBO.py"

echo '------------'
echo node-lib
node "${DIRECTORY}/node-lib/test.js"

echo '------------'
echo node-port
node "${DIRECTORY}/node-port/test.js"